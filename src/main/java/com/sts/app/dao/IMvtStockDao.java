package com.sts.app.dao;

import java.util.List;

import com.sts.app.entities.MvtStock;

public interface IMvtStockDao extends IGenericDao<MvtStock>{
	public MvtStock save(MvtStock mvtStock);
	public MvtStock update(MvtStock mvtStock);
	public List<MvtStock> selectAll();
	public List<MvtStock> selectAll(String sortField, String sort);
	public MvtStock getById(Long id);	
	public MvtStock findOne(String paramName, Object paramValue);
	public MvtStock findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
