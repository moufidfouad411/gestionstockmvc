package com.sts.app.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.sts.app.dao.IGenericDao;


@SuppressWarnings("unchecked")
public class GenericDao<E> implements IGenericDao<E> {
	@PersistenceContext
	private EntityManager em;
	
	private Class<E> type;	
	
	public GenericDao() {
		ParameterizedType pt = (ParameterizedType) getClass().getGenericSuperclass();
		this.type = (Class<E>) pt.getActualTypeArguments()[0]; 		
	}

	@Override
	public E save(E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		return em.merge(entity);
	}

	@Override
	public List<E> selectAll() {
		Query query = em.createQuery("select t from " + this.type.getSimpleName() + " t");
		return query.getResultList();
	}
	
	@Override
	public List<E> selectAll(String sortField, String sort) {
		Query query = em.createQuery("select t from " + this.type.getSimpleName() + " t order by " + sortField + " " + sort);
		return query.getResultList(); 
	}

	@Override
	public E getById(Long id) {		
		return em.find(this.type, id);
	}

	@Override
	public E findOne(String paramName, Object paramValue) {
		Query query = em.createQuery("select t from " + this.type.getSimpleName() + " t where " + paramName + " = :x");
		query.setParameter(paramName, paramValue);
		List<Object> list = query.getResultList();
		return list.size() > 0 ? (E) list.get(0) : null;
	}

	@Override
	public E findOne(String[] paramNames, Object[] paramValues) {
		String queryString = "select t from " + this.type.getSimpleName() + " t where ";
		int len = paramNames.length;
		for(int i = 0; i < len; i++ ) {
			queryString += " t." + paramNames[i] + " = :x" + i;
			if((i + 1) > len) {
				queryString += " and";
			}
		}
		
		Query query = em.createQuery(queryString);
		for(int i = 0; i < paramValues.length; i++) {
			query.setParameter("x" +  i, paramValues[i]);
		}
		List<Object> list = query.getResultList();
		return list.size() > 0 ? (E) list.get(0) : null;
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		Query query = em.createQuery("select t from " + this.type.getSimpleName() + " t where " + paramName + " = :x");
		query.setParameter(paramName, paramValue);
		List<Object> list = query.getResultList();
		return list.size() > 0 ? ((Long) query.getSingleResult()).intValue() : 0;
	}	

	@Override
	public void remove(Long id) {
		E tab = em.getReference(this.type, id);
		em.remove(tab);
	}

}
