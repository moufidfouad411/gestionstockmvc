package com.sts.app.dao;

import java.util.List;

import com.sts.app.entities.LigneVente;

public interface ILigneVenteDao extends IGenericDao<LigneVente>{
	public LigneVente save(LigneVente ligneVente);
	public LigneVente update(LigneVente ligneVente);
	public List<LigneVente> selectAll();
	public List<LigneVente> selectAll(String sortField, String sort);
	public LigneVente getById(Long id);	
	public LigneVente findOne(String paramName, Object paramValue);
	public LigneVente findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
