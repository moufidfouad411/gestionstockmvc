package com.sts.app.dao;

import java.util.List;

import com.sts.app.entities.Client;

public interface IClientDao extends IGenericDao<Client>{
	public Client save(Client client);
	public Client update(Client client);
	public List<Client> selectAll();
	public List<Client> selectAll(String sortField, String sort);
	public Client getById(Long id);	
	public Client findOne(String paramName, Object paramValue);
	public Client findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
