package com.sts.app.dao;

import java.util.List;

import com.sts.app.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {
	public Utilisateur save(Utilisateur utilisateur);
	public Utilisateur update(Utilisateur utilisateur);
	public List<Utilisateur> selectAll();
	public List<Utilisateur> selectAll(String sortField, String sort);
	public Utilisateur getById(Long id);	
	public Utilisateur findOne(String paramName, Object paramValue);
	public Utilisateur findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
