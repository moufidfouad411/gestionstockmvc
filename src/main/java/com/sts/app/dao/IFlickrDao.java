package com.sts.app.dao;

import java.io.InputStream;

import com.flickr4java.flickr.FlickrException;

public interface IFlickrDao {
	public String save(InputStream photo,String title) throws FlickrException;
}
