package com.sts.app.dao;

import java.util.List;

import com.sts.app.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient>{
	public LigneCommandeClient save(LigneCommandeClient ligneCommandeClient);
	public LigneCommandeClient update(LigneCommandeClient ligneCommandeClient);
	public List<LigneCommandeClient> selectAll();
	public List<LigneCommandeClient> selectAll(String sortField, String sort);
	public LigneCommandeClient getById(Long id);	
	public LigneCommandeClient findOne(String paramName, Object paramValue);
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
