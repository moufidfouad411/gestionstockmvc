package com.sts.app.dao;

import java.util.List;

import com.sts.app.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur>{
	public LigneCommandeFournisseur save(LigneCommandeFournisseur ligneCommandeFournisseur);
	public LigneCommandeFournisseur update(LigneCommandeFournisseur ligneCommandeFournisseur);
	public List<LigneCommandeFournisseur> selectAll();
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort);
	public LigneCommandeFournisseur getById(Long id);	
	public LigneCommandeFournisseur findOne(String paramName, Object paramValue);
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
