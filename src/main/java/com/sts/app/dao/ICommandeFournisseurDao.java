package com.sts.app.dao;

import java.util.List;

import com.sts.app.entities.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur> {
	public CommandeFournisseur save(CommandeFournisseur commandeFournisseur);
	public CommandeFournisseur update(CommandeFournisseur commandeFournisseur);
	public List<CommandeFournisseur> selectAll();
	public List<CommandeFournisseur> selectAll(String sortField, String sort);
	public CommandeFournisseur getById(Long id);	
	public CommandeFournisseur findOne(String paramName, Object paramValue);
	public CommandeFournisseur findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
