package com.sts.app.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.sts.app.dao.IVenteDao;
import com.sts.app.entities.Vente;
import com.sts.app.service.IVenteService;

@Transactional
public class VenteService implements IVenteService {
	private IVenteDao dao;

	public void setDao(IVenteDao dao) {
		this.dao = dao;
	}

	@Override
	public Vente save(Vente vente) {
		return dao.save(vente);
	}

	@Override
	public Vente update(Vente vente) {
		return dao.update(vente);
	}

	@Override
	public List<Vente> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Vente> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Vente getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public Vente findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Vente findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

}
