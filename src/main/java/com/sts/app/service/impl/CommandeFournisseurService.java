package com.sts.app.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.sts.app.dao.ICommandeFournisseurDao;
import com.sts.app.entities.CommandeFournisseur;
import com.sts.app.service.ICommandeFournisseurService;

@Transactional
public class CommandeFournisseurService implements ICommandeFournisseurService {
	
	private ICommandeFournisseurDao dao;
	
	public void setDao(ICommandeFournisseurDao dao) {
		this.dao = dao;
	}

	@Override
	public CommandeFournisseur save(CommandeFournisseur commandeFournisseur) {
		return dao.save(commandeFournisseur);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur commandeFournisseur) {
		return dao.update(commandeFournisseur);
	}

	@Override
	public List<CommandeFournisseur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<CommandeFournisseur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeFournisseur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public CommandeFournisseur findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

}
