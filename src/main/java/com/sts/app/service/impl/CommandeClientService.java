package com.sts.app.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.sts.app.dao.ICommandeClientDao;
import com.sts.app.entities.CommandeClient;
import com.sts.app.service.ICommandeClientService;

@Transactional
public class CommandeClientService implements ICommandeClientService {
	
	private ICommandeClientDao dao;
	
	public void setDao(ICommandeClientDao dao) {
		this.dao = dao;
	}

	@Override
	public CommandeClient save(CommandeClient commandeClient) {
		return dao.save(commandeClient);
	}

	@Override
	public CommandeClient update(CommandeClient commandeClient) {
		return dao.update(commandeClient);
	}

	@Override
	public List<CommandeClient> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<CommandeClient> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeClient getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public CommandeClient findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeClient findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

}
