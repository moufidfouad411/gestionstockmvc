package com.sts.app.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.sts.app.dao.IMvtStockDao;
import com.sts.app.entities.MvtStock;
import com.sts.app.service.IMvtStockService;

@Transactional
public class MvtStockService implements IMvtStockService {

	private IMvtStockDao dao;
	public void setDao(IMvtStockDao dao) {
		this.dao = dao;
	}

	@Override
	public MvtStock save(MvtStock mvtStock) {
		return dao.save(mvtStock);
	}

	@Override
	public MvtStock update(MvtStock mvtStock) {
		return dao.update(mvtStock);
	}

	@Override
	public List<MvtStock> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<MvtStock> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public MvtStock getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public MvtStock findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public MvtStock findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

}
