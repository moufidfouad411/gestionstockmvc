package com.sts.app.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.sts.app.dao.ILigneCommandeFournisseurDao;
import com.sts.app.entities.LigneCommandeFournisseur;
import com.sts.app.service.ILigneCommandeFournisseurService;

@Transactional
public class LigneCommandeFournisseurService implements ILigneCommandeFournisseurService {
	private ILigneCommandeFournisseurDao dao;
	
	public void setDao(ILigneCommandeFournisseurDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur ligneCommandeFournisseur) {
		return dao.save(ligneCommandeFournisseur);
	}

	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur ligneCommandeFournisseur) {
		return dao.update(ligneCommandeFournisseur);
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeFournisseur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public LigneCommandeFournisseur findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

}
