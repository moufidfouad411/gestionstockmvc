package com.sts.app.service;

import java.util.List;

import com.sts.app.entities.Article;

public interface IArticleService {
	public Article save(Article article);
	public Article update(Article article);
	public List<Article> selectAll();
	public List<Article> selectAll(String sortField, String sort);
	public Article getById(Long id);	
	public Article findOne(String paramName, Object paramValue);
	public Article findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
