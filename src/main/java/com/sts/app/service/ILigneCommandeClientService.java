package com.sts.app.service;

import java.util.List;

import com.sts.app.entities.LigneCommandeClient;

public interface ILigneCommandeClientService {
	public LigneCommandeClient save(LigneCommandeClient ligneCommandeClient);
	public LigneCommandeClient update(LigneCommandeClient ligneCommandeClient);
	public List<LigneCommandeClient> selectAll();
	public List<LigneCommandeClient> selectAll(String sortField, String sort);
	public LigneCommandeClient getById(Long id);	
	public LigneCommandeClient findOne(String paramName, Object paramValue);
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
