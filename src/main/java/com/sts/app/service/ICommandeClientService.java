package com.sts.app.service;

import java.util.List;

import com.sts.app.entities.CommandeClient;

public interface ICommandeClientService {
	public CommandeClient save(CommandeClient commandeClient);
	public CommandeClient update(CommandeClient commandeClient);
	public List<CommandeClient> selectAll();
	public List<CommandeClient> selectAll(String sortField, String sort);
	public CommandeClient getById(Long id);	
	public CommandeClient findOne(String paramName, Object paramValue);
	public CommandeClient findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName,Object paramValue);
	public void remove(Long id);
}
