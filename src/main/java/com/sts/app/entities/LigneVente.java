package com.sts.app.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class LigneVente implements Serializable {
	private static final long serialVersionUID = 1717609614302696617L;
	@Id
	@GeneratedValue
	private Long idLigneVente;
	
	@ManyToOne(targetEntity=Article.class)
	//@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne(targetEntity=Vente.class)
	//@JoinColumn(name = "vente")
	private Vente vente;

	public LigneVente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(Long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

}
